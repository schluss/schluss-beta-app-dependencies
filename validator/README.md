# validator

A new Flutter package.

## Getting Started

This project is a starting point for a Dart
[package](https://flutter.dev/developing-packages/),
a library module containing code that can be shared easily across
multiple Flutter or Dart projects.

For help getting started with Flutter, view our 
[online documentation](https://flutter.dev/docs), which offers tutorials, 
samples, guidance on mobile development, and a full API reference.

# Schluss validation

The official validation libarary for **Beta** application 


# Here are some supported features:

 - Common validations for schluss application
 - Pincode validation 

##  How to use

Add this to your package's pubspec.yaml file, use the latest version (2.1.1)
dependecies
```sh
logger: ^0.9.1
validator:
	path: ../validator
```
import 
```sh
import  'package:validator/validator.dart';
```
simple usage - pincode validation
```sh
PincodeValidator("12749").validate().then((val) {
	print(val);
}, onError: (e) {
	print(e);
});
```
advance usage - pincode validation
```sh
PincodeValidator("13785", // pass pincode string
	isLengthValidation:  true, // set length validation required or not
	isNumberSequenceValidation:  true,// set Number Sequence Validation required or not
	isRepeatedDigitsValidation:  true,// set Number Repeated Digits Validation required or not
	pincodeLength:  5, // set expected pincode length
).validate();
```

simple usage - common validation
```sh
print(CommonValidation().isNumeric("1234F")); // return false
```

## Advance properties

| Property| Default Value|
| ------ | ------ |
| pincodeLength| 5 |
| isLengthValidation| true |
| isRepeatedDigitsValidation | true  |
| isNumberSequenceValidation| true  |



```